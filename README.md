# ddf-models

Ce dépôt git est un sous-dépôt de ddf-data. Il comprend les vocabulaires contrôlés et  l'ontologie développée pour le Dictionnaire Des Francophones DDF. L'ontologie DDF dépend directement de vocabulaires contrôlés et de l'ontologie MNX. Elle importe également d'autres ontologies publiques via leur URL (voir fichier models/ddf.ttl).

Pour faciliter l'import, un script concatène automatiquement les fichiers .ttl dans un fichier .trig avec le graphe nommé spécifié dans la [nomenclature de graphes nommés](https://gitlab.com/mnemotix/ddf/ddf-data/-/blob/master/ddf-repository/graphes-nomm%C3%A9s-DDF.md) adoptées pour le projet. Ces fichiers sont ajoutés à la racine du projet (et aussi accessible depuis la documentation):

* Modèles et données individuelles [ddf-mnx.trig](https://gitlab.com/mnemotix/ddf/ddf-models/-/raw/master/ddf-mnx.trig?inline=false) contenant:
    * l'ontologie DDF
    * l'ontologie MNX (module de base)
* Tous les vocabulaires ainsi que les données géographiques pour le vocabulaire Place.ttl dans [vocabularies.trig](https://gitlab.com/mnemotix/ddf/ddf-models/-/raw/master/vocabularies.trig?inline=false)  

# Modifications des ontologies et des vocabulaires contrôlés

Les versions de références sont celles qui sont sur le dépôt gitlab https://gitlab.com/mnemotix/ddf/ddf-models. Pour les modifications merci de suivre la procédure :
1 . se logguer avec son compte gitlab
2. forker le dépôt
3. modifier les fichiers sources et les "pousser" sur son fork
4. soumettre un "merge request" (MR) de son fork vers le dépôt d'origine.
5. poster un message ici pour alerter de la demande de MR.

Pour l'étape 3 il est possible de repartir du [tableau Google](https://docs.google.com/spreadsheets/d/1dgHN6AavAlPZ-CvHAK07j_Yj3LeRtVckU_L07UfTgH4/edit#gid=1200549827), puis de le convertir en .ttl, avant de l'enregistrer sur son fork (edited). Pour ce faire :
*  copier-coller dans un tableur le contenu de l'onglet du tableau Vocabulaire contrôlé,
* enregistrer un fichier local. 
* Utiliser https://skos-play.sparna.fr/play/convert pour le convertir. 


# Documentations

## Ontologie DDF

Une documentation est générée automatiquement à chaque mies à jour du fichier et publiée à cette adresse : https://mnemotix.gitlab.io/ddf/ddf-models/

## Vocabulaires contrôlés

Une documentation est également générée automatiquement pour chaque vocabulaire contrôlé (sauf Places) à chaque publication d'une modification. Les liens sont les suivants:
* [Transitivity](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Transitivity/)
* [TextualGenre](https://mnemotix.gitlab.io/ddf/ddf-models/voc/TextualGenre/)
* [Lexicographic-Resource](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Lexicographic-Resource/)
* [Glossary](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Glossary/)
* [FormType](https://mnemotix.gitlab.io/ddf/ddf-models/voc/FormType/)
* [Person](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Person/)
* [Grammatical-constraint](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Grammatical-constraint/)
* [Sociolect](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Sociolect/)
* [Connotation](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Connotation/)
* [Frequency](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Frequency/)
* [Tense](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Tense/)
* [Mood](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Mood/)
* [Gender](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Gender/)
* [Temporality](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Temporality/)
* [Register](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Register/)
* [Multiword-type](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Multiword-type/)
* [Domain](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Domain/)
* [Part-of-speech-type](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Part-of-speech-type/)
* [Sense-Relation](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Sense-Relation/)
* [Number](https://mnemotix.gitlab.io/ddf/ddf-models/voc/Number/)
* [TermElement](https://mnemotix.gitlab.io/ddf/ddf-models/voc/TermElement/)
