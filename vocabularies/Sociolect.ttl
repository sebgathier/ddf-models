@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix ddf: <http://data.dictionnairedesfrancophones.org/ontology#> .
@prefix euvoc: <http://publications.europa.eu/ontology/euvoc#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix org: <http://www.w3.org/ns/org#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix skosxl: <http://www.w3.org/2008/05/skos-xl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix sociolect:  <http://data.dictionnairedesfrancophones.org/authority/sociolect/> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect> a skos:ConceptScheme;
  dct:created "2019-04-10T00:00:00.000Z"^^xsd:dateTime;
  dct:creator "Noé Gasparini, Antoine Bouchez"@fr;
  dct:description "language uses specific to a given social group or trade"@en, "usages du langage propres à un groupe social ou à un corps de métier."@fr;
  dct:isRequiredBy <http://data.dictionnairedesfrancophones.org/ontology/ddf>;
  dct:license <http://creativecommons.org/licenses/by/4.0/>;
  dct:modified "2019-04-29T00:00:00.000Z"^^xsd:dateTime;
  dct:publisher "Ddf"@fr;
  dct:rights "cc by 4.0"@fr;
  dct:title "Sociolect"@en, "Sociolecte"@fr;
  dct:type "Vocabulaire contrôlé"@fr;
  skos:hasTopConcept <http://data.dictionnairedesfrancophones.org/authority/sociolect/butcherSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/dockerSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/europeanUnionSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/financeSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/fleaMarketSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/internetSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/journalistSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/militarySociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/physicianSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/poiluSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/policeSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/politicianSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/postOfficeSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/railwayorkerSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/schoolSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/taxiSociolect>, <http://data.dictionnairedesfrancophones.org/authority/sociolect/thiefSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/truckDriverSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/typographerSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/unspecifiedSociolect>,
    <http://data.dictionnairedesfrancophones.org/authority/sociolect/youngsterSociolect>;
  skos:prefLabel "Sociolecte"@fr .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/unspecifiedSociolect>
  a skos:Concept;
  skos:definition "utilisé dans certains cadre de discussion"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:notation "[[Catégorie:Argots en français‎]]"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "arg."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>, "{{argot|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "argot"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/schoolSociolect>
  a skos:Concept;
  skos:definition "utilisé par les étudiants et élèves"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:notation "[[Catégorie:Argot scolaire en français‎]]"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "arg. scol."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>, "arg. étud."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>,
    "lang. étud."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>,
    "{{argot scolaire|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "argot scolaire"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/policeSociolect>
  a skos:Concept;
  skos:definition "utilisé par les policiers"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:notation "[[Catégorie:Argot scolaire en français‎]]"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{argot policier|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "argot policier"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/internetSociolect>
  a skos:Concept;
  skos:definition "utilisé sur le web"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:notation "{{argot Internet}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{argot internet|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "argot d'Internet"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/financeSociolect>
  a skos:Concept;
  skos:definition "utilisé par les gens travaillant dans la finance"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot de la finance"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/butcherSociolect>
  a skos:Concept;
  skos:definition "utilisé par les bouchers et charcutiers"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:notation "[[Catégorie:Louchébem‎]]"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{louchébem}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "argot des bouchers"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/fleaMarketSociolect>
  a skos:Concept;
  skos:definition "utilisé par les brocanteurs"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des brocanteurs"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/railwayorkerSociolect>
  a skos:Concept;
  skos:definition "utilisé par les gens travaillant dans les chemins de fer et compagnies ferroviaires"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des cheminots"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/dockerSociolect>
  a skos:Concept;
  skos:definition "utilisé par les gens travaillant dans les ports"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des dockers"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/postOfficeSociolect>
  a skos:Concept;
  skos:definition "utilisé par les postes et facteurs"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des facteurs"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/journalistSociolect>
  a skos:Concept;
  skos:definition "utilisé par les journalistes"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des journalistes"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/physicianSociolect>
  a skos:Concept;
  skos:definition "utilisé par les médecins"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des médecins"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/politicianSociolect>
  a skos:Concept;
  skos:definition "utilisé par les gens travaillant en politique"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des politiciens"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/truckDriverSociolect>
  a skos:Concept;
  skos:definition "utilisé par les routiers"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des routiers"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/taxiSociolect> a
    skos:Concept;
  skos:definition "utilisé par les chauffeurs de taxi"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des taxis"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/typographerSociolect>
  a skos:Concept;
  skos:definition "utilisé par les typographes et imprimeurs"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:notation "[[Catégorie:Argot des typographes en français‎]]"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{argot typographes|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "argot des typographes"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/militarySociolect>
  a skos:Concept;
  skos:definition "utilisé par les militaires"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:notation "[[Catégorie:Argot militaire en français‎]]"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{argot militaire|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "argot militaire"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/poiluSociolect> a
    skos:Concept;
  skos:definition "autrefois utilisé par les soldats français pendant la Première Guerre mondiale"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot poilu"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/europeanUnionSociolect>
  a skos:Concept;
  skos:definition "utilisé par les fonctionnaires de l'Union Européenne"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot de l'Union Européenne"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/thiefSociolect> a
    skos:Concept;
  skos:definition "utilisé par les voleurs, les malfrats, les bandits"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des voleurs"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .

<http://data.dictionnairedesfrancophones.org/authority/sociolect/youngsterSociolect>
  a skos:Concept;
  skos:definition "utilisé par les adolescents et jeunes adultes"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sociolect>;
  skos:prefLabel "argot des jeunes"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/sociolect> .
